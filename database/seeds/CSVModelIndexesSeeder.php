<?php

use Illuminate\Database\Seeder;

class CSVModelIndexesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $indexes = [
            ['name' => 'zip', 'model' => 'city'],
            ['name' => 'lat', 'model' => 'city'],
            ['name' => 'lng', 'model' => 'city'],
            ['name' => 'city', 'model' => 'city'],
            ['name' => 'population', 'model' => 'city'],
            ['name' => 'density', 'model' => 'city'],
            ['name' => 'timezone', 'model' => 'city'],

            ['name' => 'county_fips', 'model' => 'county'],
            ['name' => 'county_name', 'model' => 'county'],
            ['name' => 'county_weights', 'model' => 'county'],
            ['name' => 'county_names_all', 'model' => 'county'],
            ['name' => 'county_fips_all', 'model' => 'county'],

            ['name' => 'state_id', 'model' => 'state'],
            ['name' => 'state_name', 'model' => 'state'],
        ];

        foreach ($indexes as $index) {
            \App\Model\CSVModelIndexes::create($index);
        }

    }
}
