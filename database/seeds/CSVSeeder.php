<?php

use App\Utils\ImportCSV;
use Illuminate\Database\Seeder;
use \App\Utils;
class CSVSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $import = new ImportCSV(public_path('uszips.csv'));
        $import->prepare()
                ->execute();
    }
}
