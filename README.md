
## Deploy instruction
git clone https://lanky_89@bitbucket.org/lanky_89/import-api.git

 

Rename .env.example file to .env

|                       |
| ----------------------|
| composer install |
  

|                       |
| ----------------------|
|php artisan migrate  |

|                       |
| ----------------------|
|php artisan queue:work --daemon  

|                       |
| ----------------------|
|php artisan storage:link

|                       |
| ----------------------|
|php artisan db:seed --class=CSVSeeder


# API USAGE

___

### Upload CSV file

***

> **POST** {{url}}/api/upload/csv

##### Parameters

| Key                                                                                            | Description                     |
| ---------------------------------------------------------------------------------------------- | ------------------------------- |
| *file* `(file, required)` | Must be CSV file with headers: zip, lat, lng, city, state_id, state_name, zcta, parent_zcta, population, density, county_fips, county_name, county_weights, county_names_all, county_fips_all, imprecise, military, timezone |

##### Response Example

```json
{
"status":true,
"data":null
}
```

___

### Getting location by zip

***

> **GET** {{url}}/api/search/zip?zip="zipNumber"

##### Parameters

| Key                                                                                            | Description                     |
| ---------------------------------------------------------------------------------------------- | ------------------------------- |
| *zip* `(string, required)` | Zip number of city |

##### Response Example

```json
{
    "status": true,
    "data": {
        "city": {
            "id": 19,
            "name": "Florida",
            "county_id": 19,
            "zip": "00650",
            "lat": "18.34321",
            "lng": "-66.58601",
            "timezone": "America/Puerto_Rico",
            "population": 14534,
            "density": 183.9,
            "created_at": "2021-03-10T07:53:46.000000Z",
            "updated_at": "2021-03-10T07:53:46.000000Z"
        },
        "county": {
            "id": 19,
            "name": "Florida",
            "state_id": 1,
            "fips": 72054,
            "weights": "{\"72013\": \"1.51\", \"72017\": \"1.91\", \"72039\": \"1.13\", \"72054\": \"83.61\", \"72141\": \"11.84\"}",
            "names_all": "Florida|Utuado|Barceloneta|Arecibo|Ciales",
            "fips_all": "72054|72141|72017|72013|72039",
            "created_at": "2021-03-10T07:53:46.000000Z",
            "updated_at": "2021-03-10T07:53:46.000000Z"
        },
        "state": {
            "id": 1,
            "name": "Puerto Rico",
            "code": "PR",
            "created_at": "2021-03-10T07:53:46.000000Z",
            "updated_at": "2021-03-10T07:53:46.000000Z"
        }
    }
}
```

___

### Getting location by city name

***

> **GET** {{url}}/api/search/city?city=Ada&limit=3&offset=1

##### Parameters

| Key                                                                                            | Description                     |
| ---------------------------------------------------------------------------------------------- | ------------------------------- |
| *city* `(string, required, min:2 characters)` | City name  |
| *offset* `(numeric, required)` | Offset of the result  |
| *limit* `(numeric, required)` | Limit of the result  |

##### Response Example

```json
{
    "status": true,
    "data": {
        "0": {
            "city": {
                "id": 18,
                "name": "Ensenada",
                "county_id": 16,
                "zip": "00647",
                "lat": "17.96612",
                "lng": "-66.94383",
                "timezone": "America/Puerto_Rico",
                "population": 5809,
                "density": 147.5
            },
            "county": {
                "id": 16,
                "name": "Guánica",
                "state_id": 1,
                "fips": 72055,
                "weights": "{\"72055\": \"98.88\", \"72079\": \"1.12\"}",
                "names_all": "Guánica|Lajas",
                "fips_all": "72055|72079"
            },
            "state": {
                "id": 1,
                "name": "Puerto Rico",
                "code": "PR"
            }
        },
        "1": {
            "city": {
                "id": 219,
                "name": "Adams",
                "county_id": 85,
                "zip": "01220",
                "lat": "42.62372",
                "lng": "-73.11718",
                "timezone": "America/New_York",
                "population": 8179,
                "density": 128.4
            },
            "county": {
                "id": 85,
                "name": "Berkshire",
                "state_id": 3,
                "fips": 25003,
                "weights": "{\"25003\": \"100\"}",
                "names_all": "Berkshire",
                "fips_all": "25003"
            },
            "state": {
                "id": 3,
                "name": "Massachusetts",
                "code": "MA"
            }
        },
        "2": {
            "city": {
                "id": 236,
                "name": "North Adams",
                "county_id": 85,
                "zip": "01247",
                "lat": "42.69547",
                "lng": "-73.07997",
                "timezone": "America/New_York",
                "population": 15466,
                "density": 113.7
            },
            "county": {
                "id": 85,
                "name": "Berkshire",
                "state_id": 3,
                "fips": 25003,
                "weights": "{\"25003\": \"100\"}",
                "names_all": "Berkshire",
                "fips_all": "25003"
            },
            "state": {
                "id": 3,
                "name": "Massachusetts",
                "code": "MA"
            }
        },
        "limit": "3",
        "offset": "1"
    }
}
```
