<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('upload/csv', [\App\Http\Controllers\Api\v1\ImportController::class, 'uploadFile'])->name('upload');
Route::get('search/zip', [\App\Http\Controllers\Api\v1\SearchController::class, 'searchByZip'])->name('searchByZip');
Route::get('search/city', [\App\Http\Controllers\Api\v1\SearchController::class, 'searchByCity'])->name('searchByCity');
