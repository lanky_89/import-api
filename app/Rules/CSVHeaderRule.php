<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Http\UploadedFile;
use League\Csv\Reader;
use League\Csv\Statement;

class CSVHeaderRule implements Rule
{

    protected $template = [
        0 => "zip",
        1 => "lat",
        2 => "lng",
        3 => "city",
        4 => "state_id",
        5 => "state_name",
        6 => "zcta",
        7 => "parent_zcta",
        8 => "population",
        9 => "density",
        10 => "county_fips",
        11 => "county_name",
        12 => "county_weights",
        13 => "county_names_all",
        14 => "county_fips_all",
        15 => "imprecise",
        16 => "military",
        17 => "timezone",
    ];

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if ($this->checkIsCSVFile($value)) {
            $result = $this->prepareDataToValidate($value);
            return $this->checkCompareHeader($result->getHeader());
        }
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The file has the wrong structure.';
    }

    private function checkCompareHeader($data)
    {
        $result = array_uintersect($data, $this->template, "strcasecmp"); // https://www.php.net/manual/en/function.array-uintersect.php
        return $result == $this->template;
    }

    private function prepareDataToValidate($value)
    {
        $csv = Reader::createFromFileObject(new \SplFileObject($value, 'a+'));
        $csv->setHeaderOffset(0);

        $statement = new Statement();
        $stmt = $statement->limit(1);

        return $stmt->process($csv);
    }

    private function checkIsCSVFile($value)
    {
        if ($value instanceof UploadedFile) {
            return $value->getClientOriginalExtension() == 'csv' ?? false;
        }

        return false;
    }
}
