<?php

namespace App\Utils;

use App\Jobs\InsertCSVData;
use \App\Utils\Abstracts;
use League\Csv\Reader;
use League\Csv\Statement;

class ImportCSV extends Abstracts\Import
{
    protected $dirSave;
    protected $records;
    protected $dataToQueue;
    public $path;

    public function __construct($path = null)
    {
        $this->path = $path ?? public_path('csv');
        $this->dirSave = 'csv';
    }

    public function getDataToQueue()
    {
        return $this->dataToQueue;
    }

    public function prepare()
    {
        $csv = Reader::createFromPath($this->path, 'r');
        $csv->setHeaderOffset(0);

        $statement = new Statement();
        $this->records = $statement->process($csv);

        return $this;
    }

    public function execute()
    {
        $dataToQueue = [];
        foreach ($this->records as $record) {
            if (count($dataToQueue) < config('app.count_data_to_queue')) {
                $dataToQueue[] = $record;
            } else {
                $this->dataToQueue = $dataToQueue;
                dispatch(new InsertCSVData($this));
                $dataToQueue = [];
            }
        }
        return true;
    }
}
