<?php


namespace App\Utils;


class Response
{
    /**
     * @param null $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function handlingResponse($data = null)
    {
        return response()->json([
            'status' => true,
            'data' => $data
        ], 200);
    }

    /**
     * @param $messsage
     * @return \Illuminate\Http\JsonResponse
     */
    public function errorResponse($messsage)
    {
        return response()->json([
            'status' => false,
            'message' => $messsage
        ], 419);
    }

}
