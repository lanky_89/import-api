<?php


namespace App\Utils\Abstracts;


use Faker\Provider\File;
use Illuminate\Support\Facades\Storage;

abstract class Import
{
    protected $dirSave;
    protected $path;

    public abstract function prepare();

    public abstract function execute();

    public function saveFile($request)
    {

        if ($request->file('file')) {
            $filenameWithExt = $request->file('file')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('file')->getClientOriginalExtension();
            $fileNameToStore = $filename . '_' . time() . '.' . $extension;

            $path = $this->dirSave . '/' . $fileNameToStore;
            $result = Storage::disk('public')->put($path, $request->file('file')->getContent());

            if ($result) {
                $this->path = storage_path('app/public/' . $path);
            }

            return $this;
        }

    }
}
