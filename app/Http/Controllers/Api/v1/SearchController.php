<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\CityRequest;
use App\Http\Requests\ZipRequest;
use App\Model\City;
use App\Utils\Response;
use Illuminate\Support\Collection;

class SearchController extends Controller
{

    protected $response;

    public function __construct(Response $response)
    {
        $this->response = $response;
    }

    public function searchByZip(ZipRequest $request)
    {
        $locationData = City::getByCriteria(
            'zip',
            '=',
            $request->get('zip'),
            [
                'limit' => $request->get('limit'),
                'offset' => $request->get('offset'),
            ]);
        $locationData = $locationData->isNotEmpty() ? $locationData[0]->restruct() : null;

        return $this->response->handlingResponse($locationData);
    }

    public function searchByCity(CityRequest $request)
    {
        $locationData = City::getByCriteria(
            'name',
            'LIKE',
            '%' . $request->get('city') . '%',
            [
                'limit' => $request->get('limit'),
                'offset' => $request->get('offset'),
            ]);

        $locationData = $locationData->isNotEmpty() ? $this->toRestruct($locationData) : null;

        $locationData['limit'] = $request->get('limit');
        $locationData['offset'] = $request->get('offset');

        return $this->response->handlingResponse($locationData);

    }

    private function toRestruct($locationData)
    {
        $result = [];
        foreach ($locationData as $data) {
            $result[] = $data->restruct();
        }

        return $result;
    }
}
