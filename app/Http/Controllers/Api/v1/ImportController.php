<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\CSVRequest;
use App\Utils\ImportCSV;
use App\Utils\Response;
use Illuminate\Http\Request;

class ImportController extends Controller
{
    protected $response;

    public function __construct(Response $response)
    {
        $this->response = $response;
    }

    public function uploadFile(CSVRequest $request)
    {
        $file = $request->file('file');
        switch ($file->getClientOriginalExtension()) {
            case 'csv':
                $import = new ImportCSV();
                $import->saveFile($request)
                    ->prepare()
                    ->execute();

                return $this->response->handlingResponse();
        }
    }

}
