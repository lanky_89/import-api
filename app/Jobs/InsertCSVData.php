<?php

namespace App\Jobs;

use App\Model\State;
use App\Utils\ImportCSV;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class InsertCSVData implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $dataToQueue;
    protected $importCSV;

    /**
     * InsertCSVData constructor.
     * @param $importCSV
     */
    public function __construct(ImportCSV $importCSV)
    {
        $this->importCSV = $importCSV;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        foreach ($this->importCSV->getDataToQueue() as $item) {

            DB::transaction(function () use ($item) {
                $state = State::with('counties')->firstOrCreate([
                    'name' => $item['state_name'],
                    'code' => $item['state_id'],
                ]);

                $counties = $state->counties()->firstOrCreate(
                    ['fips' => $item['county_fips'], 'name' => $item['county_name']],
                    [
                        'weights' => $item['county_weights'],
                        'names_all' => $item['county_names_all'],
                        'fips_all' => $item['county_fips_all']
                    ]);

                $counties->cities()->firstOrCreate(
                    ['zip' => $item['zip'], 'name' => $item['city']],
                    [
                        'lat' => $item['lat'],
                        'lng' => $item['lng'],
                        'population' => (int)$item['population'],
                        'density' => (float)$item['density'],
                        'timezone' => $item['timezone']
                    ]);
            }, 5);
        }

    }
}
