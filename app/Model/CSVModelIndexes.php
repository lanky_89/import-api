<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CSVModelIndexes extends Model
{
    public $table = 'csv_model_indexes';
}
