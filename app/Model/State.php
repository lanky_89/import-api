<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{

    protected $fillable = [
        'name',
        'code'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    public function counties()
    {
        return $this->hasMany(County::class);
    }
}
