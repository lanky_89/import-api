<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $fillable = [
      'zip',
      'lat',
      'lng',
      'name',
      'population',
      'density',
      'timezone',
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    public function county()
    {
        return $this->belongsTo(County::class);
    }

    public function restruct()
    {
        $county = $this->county;
        $state = $this->county->state;

        unset($this->county);
        unset($this->state);
        unset($county->state);

        return [
            'city' => $this,
            'county' => $county,
            'state' => $state
        ];
    }

    public static function getByCriteria($field, $operator, $value, array $options = [])
    {
        $locationData = self::where($field, $operator, $value)
            ->with('county.state')
            ->limit(isset($options['limit']) ? $options['limit'] : 1)
            ->offset(isset($options['offset']) ? $options['offset'] : 0)
            ->get();

        return $locationData;
    }
}
