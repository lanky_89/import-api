<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class County extends Model
{
    protected $fillable = [
      'fips',
      'name',
      'weights',
      'names_all',
      'fips_all',
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function state()
    {
        return $this->belongsTo(State::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cities()
    {
        return $this->hasMany(City::class);
    }
}
